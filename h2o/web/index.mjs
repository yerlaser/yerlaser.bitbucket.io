import {Backend} from './backend.mjs'
import {Utils} from '../utils.mjs'
import {Meta} from './meta.mjs'
import {Model} from './model.mjs'
import {View} from './view.mjs'

const values = ['heaterRoom', 'coolerRoom', 'heaterWater', 'coolerWater', 'light', 'ventilator', 'ozonator', 'pump',
  'valve1', 'valve2', 'valve3', 'valve4', 'valve5', 'valve6', 'valve7', 'valve8',
  'temperatureRoom', 'temperatureWater', 'time']
const metadata = ['manualMode', 'dataInitComplete', 'readError', 'rtcError',
  'dhtError', 'dallasError', 'activeLow', 'valveMaxCount', 'valveCount', 'sensorCount']

const utils = new Utils()
const view = new View()
const model = new Model()
const meta = new Meta()
const backend = new Backend()

utils.init('192.168.4.1')
utils.debug(true)
view.init(values, values, utils)
model.init(values, view)
meta.init(metadata, model, view, utils)
backend.init(model, meta, values, metadata, utils)

backend.send('RMN')
setInterval(() => backend.send('RMQ'), 7 * 1000)
