self.addEventListener('install', (event) => {
  const cacheName = 'cachev1'
  const urlsToCache = [
    '../../favicon.ico',
    '../utils.mjs',
    './',
    './backend.mjs',
    './controller.mjs',
    './favicon-1024.png',
    './favicon-256.png',
    './index.css',
    './index.html',
    './index.mjs',
    './manifest.json',
    './meta.mjs',
    './model.mjs',
    './swreg.js',
    './view.mjs',
    './zzspinner.css'
  ]
  event.waitUntil(caches.open(cacheName).then(
    cache => cache.addAll(urlsToCache)
  ).catch(
    e => console.log(e)
  ))
})

//self.addEventListener('activate', (event) => {
//  const cacheWhitelist = ['cachev1']
//  event.waitUntil(
//    caches.keys().then(cacheNames => {
//      return Promise.all(
//        cacheNames.map(cacheName => {
//          if (cacheWhitelist.indexOf(cacheName) === -1) {
//            return caches.delete(cacheName)
//          }
//        })
//      )
//    })
//  )
//})

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request).then(response => (response || fetch(event.request).catch(e => {
      console.log('Fetching from Service Worker failed')
    })))
  )
})