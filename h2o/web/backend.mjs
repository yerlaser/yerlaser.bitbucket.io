class Backend {
  #model
  #meta
  #values
  #metadata
  #utils

  init(model, meta, values, metadata, utils) {
    this.#model = model
    this.#meta = meta
    this.#values = values
    this.#metadata = metadata
    this.#utils = utils
  }

  processRaw(str) {
    if (str === null) {
      this.#model.blend(true)
      return
    } else if (str.length < 1) {
      return
    }

    this.#model.blend(false)
    let json
    if ((/(?:[{])([A-Z0-9_]+)(?:[:]['])([A-Za-z0-9_ ,-]+)(?:['][}])/).test(str)) {
      json = str.replace(/(?:.*[{])([A-Z0-9_]+)(?:[:]['])([A-Za-z0-9_ ,-]+)(?:['][}].*)/, '{"$1":"$2"}')
    } else {
      this.#utils.log(`Garbled input: ${str}`)
      return
    }

    try {
      json = JSON.parse(json)
    } catch (e) {
      this.#utils.log(`Cannot parse: '${rawJSON}'`)
      return
    }

    const [key] = Object.keys(json)
    const value = json[key]
    switch (key) {
      case 'N':
        this.#metadata.forEach((k, i) => {
          if (i < 7) {
            this.#meta.setData(k, value[i])
          } else {
            this.#meta.setData(k, parseInt(value[i]))
          }
        })
        break
      case 'Q':
        const valueArray = value.split('_')

        let t1 = parseInt(valueArray[0])
        let t2 = t1 % 3600
        t1 = Math.floor(t1 / 3600)
        const h = `${t1 < 10 ? '0' : ''}${t1}`

        t1 = t2
        t2 = t1 % 60
        t1 = Math.floor(t1 / 60)
        const m = `${t1 < 10 ? '0' : ''}${t1}`
        const s = `${t2 < 10 ? '0' : ''}${t2}`
        this.#model.time = `${h}:${m}:${s}`

        t1 = parseInt(valueArray[5])
        if (t1 > 0) {
          t2 = t1
          this.#meta.setData('sensorCount', Math.floor(t2 / 100000))
          t2 %= 100000
          this.#meta.setData('valveCount', Math.floor(t2 / 10000))
          t2 %= 10000
          this.#meta.setData('valveMaxCount', Math.floor(t2 / 1000))
          t2 %= 1000
          for (let i = 7; i > 0; i--) {
            this.#meta.setData(this.#metadata[i - 1], Math.floor(t2 / Math.pow(2, i - 1)))
            t2 %= Math.pow(2, i - 1)
          }
        }

        let bitMask = `${parseInt(valueArray[1]).toString(2).split('').reverse().join('')}0000000`.slice(0, 8)
        bitMask += `${parseInt(valueArray[2]).toString(2).split('').reverse().join('')}0000000`.slice(0, 8)

        if (this.#meta.activeLow === '1') {
          bitMask = bitMask.split('').map((x) => (x === '0' ? '1' : '0')).join('')
        }

        this.#utils.log(`bitMask='${bitMask}'`)

        this.#values.forEach((k, i) => {
          if (i < bitMask.length) {
            this.#model[k] = (bitMask[i] === '1') ? ' ' : ''
          }
        })

        t1 = parseInt(valueArray[4])
        if (isNaN(t1) || t1 < 1) {
          this.#model.temperatureRoom = '-'
        } else {
          this.#model.temperatureRoom = t1.toString()
        }

        t1 = parseInt(valueArray[6])
        if (isNaN(t1) || t1 < 1) {
          this.#model.temperatureWater = '-'
        } else {
          this.#model.temperatureWater = t1.toString()
        }
        break
      default:
        break
    }
  }

  async send(cmd) {
    try {
      this.processRaw(await this.#utils.send(cmd))
    } catch(e) {
      this.#utils.log(e)
    }
  }
}

export {Backend}
