class Utils {
  #log
  #ip
  #url

  init(ip) {
    this.#ip = ip
    this.#url = `http://${this.#ip}/j?c=`
  }

  debug(log) {
    this.#log = log
  }

  log(msg) {
    if (this.#log) {
      console.log(msg)
    }
  }

  async send(cmd) {
    try {
      const response = await fetch(`${this.#url}${cmd}\n`, {cache: 'no-cache'})
      this.log(`Sent: '${cmd}'`)
      if (response.ok) {
        return await response.text()
      } else {
        this.log(`Status=${response.status}`)
      }
    } catch(e) {
      this.log(`Other Error:'''${e}'''`)
      return null
    }
  }
}

export {Utils}
